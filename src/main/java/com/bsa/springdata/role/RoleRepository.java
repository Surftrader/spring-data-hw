package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Transactional
    @Query(value = "DELETE FROM roles AS r WHERE r.code LIKE :code AND NOT EXISTS (SELECT ur FROM user2role ur WHERE ur.role_id=r.id)", nativeQuery = true)
    @Modifying
    void removeDistinctByCode(String code);

}
