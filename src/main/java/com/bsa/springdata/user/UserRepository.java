package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    @Query("SELECT u FROM User u WHERE LOWER(u.lastName) LIKE LOWER(CONCAT('%',:lastName,'%')) ORDER BY u.lastName ASC")
    List<User> findUsersByLastName(String lastName, Pageable pageable);

    @Query("SELECT u FROM Office o JOIN User u ON o.id=u.office.id WHERE o.city LIKE ?1 ORDER BY u.lastName ASC")
    List<User> findAllByCity(String city);

    @Query("SELECT u FROM User u WHERE u.experience >= :experience ORDER BY u.experience DESC")
    List<User> findAllByExperienceMoreOrEqualsSortDesc(int experience);

    @Query("SELECT u FROM User u JOIN Office o ON u.office.id=o.id JOIN Team t ON u.team.id=t.id WHERE o.city LIKE :city AND t.room LIKE :room")
    List<User> findAllByCityAndRoomOrderByLastName(String city, String room, Sort sort);

    @Modifying
    @Query(value = "DELETE FROM User u WHERE u.experience < :experience")
    int deleteUsersByExperience(int experience);

}
